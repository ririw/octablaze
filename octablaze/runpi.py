from octablaze.restep import blocker, radial_planner, steppers

stepper_interfaces = {
    "ns": steppers.DummyInterface(
        step_ctrl_pin=4,  # Yellow
        enable_ctrl_pin=3,  # Green
        clockwise_ctrl_pin=2,  # Red
        clockwise_high=False,
    ),
    "ew": steppers.DummyInterface(
        step_ctrl_pin=15,  # Yellow
        enable_ctrl_pin=14,  # Green
        clockwise_ctrl_pin=18,  # Red
        clockwise_high=False,
    ),
}

wire = radial_planner.WireConfig(10, 5, 1, 8, 2)
# noinspection DuplicatedCode
stepper_cfg = [
    blocker.StepperConfig(0, "ns", True, 200),
    blocker.StepperConfig(0, "ew", True, 200),
]
steps = radial_planner.steps_to_posn(
    radial_planner.AnglePosition(0, 0),
    radial_planner.AnglePosition(10, 10),
    wire,
    stepper_cfg,
)
steppers.StepBlock(steps, stepper_interfaces).run_block()

steps = radial_planner.steps_to_posn(
    radial_planner.AnglePosition(10, 10),
    radial_planner.AnglePosition(0, 0),
    wire,
    stepper_cfg,
)
steppers.StepBlock(steps, stepper_interfaces).run_block()
