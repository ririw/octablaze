"""
Planning based on moving to a given angle

Assumption
==========
We'll assume that our tentacle basically forms a circle-shape
when it moves to a particular spot, and that the arm will not
twist at all. Given that knowledge, it is possible to calculate
the lengths of the wires, which we can feed into the step plans.

Mathematics
===========

Inputs:
 - L: Tentacle length
 - O: Vertebra offsets
 - z: rotation of the y axis around the z axis
 - y: rotation around the y axis

"""
import typing as ty

import attr
import numpy as np

from octablaze.restep import accel_planner, blocker, steppers


@attr.s
class AnglePosition:
    z_rot_deg: float = attr.ib()
    y_rot_deg: float = attr.ib()


def calc_dlen(octalen, voffset, alpha_prop):
    r = octalen / (2 * np.pi * alpha_prop)
    a_len = r - voffset
    return 2 * np.pi * a_len * alpha_prop


def calc_angle_offset(z_rot_prop, voffset):
    return np.cos(z_rot_prop * 2 * np.pi) * voffset


def calc_lengths(octalen, voffset, z_rot_deg, y_rot_deg):
    prop_z = z_rot_deg / 360
    prop_y = y_rot_deg / 360

    angle_offset = calc_angle_offset(prop_z, voffset)
    if prop_y == 0:
        return octalen, octalen

    return calc_dlen(octalen, angle_offset, prop_y), calc_dlen(
        octalen, -angle_offset, prop_y
    )


@attr.s
class FullPosition:
    north_length = attr.ib()
    south_length = attr.ib()
    east_length = attr.ib()
    west_length = attr.ib()


def calc_full_posn(octalen, voffset, z_rot_deg, y_rot_deg) -> FullPosition:
    north_length, south_length = calc_lengths(octalen, voffset, z_rot_deg, y_rot_deg)
    east_length, west_length = calc_lengths(octalen, voffset, z_rot_deg + 90, y_rot_deg)

    return FullPosition(
        north_length=north_length,
        south_length=south_length,
        east_length=east_length,
        west_length=west_length,
    )


@attr.s
class WireConfig:
    steps_per_mm: int = attr.ib()
    octalen_mm: float = attr.ib()
    offset_mm: float = attr.ib()
    max_speed_mm_s: float = attr.ib(default=50)
    max_accel_mm_ss: float = attr.ib(default=250)


def steps_to_posn(
    current_position: AnglePosition,
    to_position: AnglePosition,
    wireconf: WireConfig,
    stepper_conf: ty.List[blocker.StepperConfig],
) -> ty.List[steppers.Step]:
    current_position_len = calc_full_posn(
        wireconf.octalen_mm,
        wireconf.offset_mm,
        current_position.z_rot_deg,
        current_position.y_rot_deg,
    )
    new_position_len = calc_full_posn(
        wireconf.octalen_mm,
        wireconf.offset_mm,
        to_position.z_rot_deg,
        to_position.y_rot_deg,
    )

    dn_mm = current_position_len.north_length - new_position_len.north_length
    ds_mm = current_position_len.south_length - new_position_len.south_length
    dw_mm = current_position_len.west_length - new_position_len.west_length
    de_mm = current_position_len.east_length - new_position_len.east_length
    assert np.isclose(
        dn_mm, -ds_mm
    ), "Could not reconcile length difference: {} vs {}".format(dn_mm, -ds_mm)
    assert np.isclose(
        dw_mm, -de_mm
    ), "Could not reconcile length difference: {} vs {}".format(dw_mm, -de_mm)

    plan = accel_planner.plan_from_to(
        accel_planner.PlannerPoint(
            [current_position_len.north_length, current_position_len.west_length]
        ),
        accel_planner.PlannerPoint(
            [new_position_len.north_length, new_position_len.west_length]
        ),
        wireconf.max_speed_mm_s,
        wireconf.max_accel_mm_ss,
    )

    steps = blocker.fully_plan(plan, stepper_conf)
    return steps
