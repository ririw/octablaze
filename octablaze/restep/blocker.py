import datetime
import logging
import pathlib
import pickle
import typing as ty

import attr
import attrs
import dateutil.tz
import numpy as np
from scipy import interpolate, optimize

from octablaze import cfg
from octablaze.restep import accel_planner, steppers


@attr.s()
class StepperConfig:
    axis: int = attr.ib()
    name: str = attr.ib()
    pos_clockwise: bool = attr.ib()
    steps_per_mm: int = attr.ib()


class NoNextStepException(Exception):
    pass


class NoStepSolnException(Exception):
    pass


class NoStepDirection(Exception):
    pass


class PlanStepsMismatch(Exception):
    pass


def manual_solve(r_poly, now, next_step_, steps_per_mm):
    r_grad = r_poly.derivative()
    bounds = [[now, np.inf]]

    def err(x):
        (x,) = x
        e = r_poly(x) - next_step_ / steps_per_mm
        return e**2

    def grad(x):
        (x,) = x
        return [r_grad(x)]

    res = optimize.minimize(err, np.array([now]), jac=grad, bounds=bounds)
    if not res.success:
        raise ValueError("failure to solve")
    return res.x[0]


def next_step(
    r_poly: interpolate.PPoly, s_cfg: StepperConfig, now: float
) -> (float, int):
    if now > r_poly.x.max():
        raise NoNextStepException(
            f"No next step for axis [axis={s_cfg.axis} or {s_cfg.name}, time={now}]"
        )
    current_location = np.round(r_poly(now), 15)
    next_direction = find_next_direction(current_location, now, r_poly)
    current_step = np.floor(current_location * s_cfg.steps_per_mm)
    next_step_ = current_step + next_direction

    try:
        target = next_step_ / s_cfg.steps_per_mm
        assert not np.isnan(target)
        next_step_times = r_poly.solve(target)
    except RuntimeError:
        logging.error("solve failure, falling back to optimisation")
        next_step_time = manual_solve(r_poly, now, next_step_, s_cfg.steps_per_mm)
        return next_step_time, next_direction
    next_step_times_filt = next_step_times[next_step_times >= now]
    if not next_step_times_filt.size:
        max_step = r_poly(r_poly.x[-1])
        if np.isclose(next_step_ / s_cfg.steps_per_mm, max_step, rtol=1e-3, atol=0.01):
            return r_poly.x[-1], next_direction
        raise NoStepSolnException()

    closest_next = np.argmin(np.abs(next_step_times_filt - now))
    step_t = next_step_times_filt[closest_next]
    return step_t, next_direction


def find_next_direction(current_location, now, r_poly, delta=0.0001):
    if delta >= 0.1:
        raise NoStepDirection(
            f"Invalid delta: {delta} at {current_location} and time {now}"
        )
    next_direction = np.sign(r_poly(now + delta) - current_location)
    if np.isnan(next_direction):
        next_direction = np.sign(current_location - r_poly(now - delta))
    if next_direction == 0:
        next_direction = find_next_direction(current_location, now, r_poly, delta * 10)

    if next_direction == 0 or np.isnan(next_direction):
        raise NoStepDirection(
            f"Invalid direction: {next_direction} at {current_location} and time {now}"
        )
    return next_direction


def plan_to_blocks(
    plan: accel_planner.RunPlan, stepper_cfg: StepperConfig
) -> ty.List[steppers.Step]:
    now = 0
    steps = []
    end_time = plan.t
    poly = plan.r_poly
    # ignore constant term during summation.
    if abs(poly.c[:-1]).sum() == 0:
        return []

    total_steps = int(
        np.round((plan.r_poly(plan.t) - plan.r_poly(0)) * stepper_cfg.steps_per_mm)
    )
    while now < end_time:
        try:
            next_time, next_direction = next_step(poly, stepper_cfg, now)
        except NoNextStepException:
            logging.info(f"Finished axis early [now={now}]")
            break

        if next_time == now:
            # Skip "pauses" where the time of the step is unchanged.
            # these are slow movements, so we don't actually want to
            # step at all. For example, inflection points.
            now += 1e-8
            continue
        if stepper_cfg.pos_clockwise:
            is_clockwise = next_direction == 1
        else:
            is_clockwise = next_direction == -1
        step = steppers.Step(
            stepper=stepper_cfg.name,
            dtime=next_time - now,
            time=now,
            is_clockwise=is_clockwise,
        )
        steps.append(step)
        now = next_time
    if abs(len(steps) - total_steps) / total_steps > 1e-3:
        raise PlanStepsMismatch()
    steps = steps[:total_steps]
    return steps


def fully_plan(
    plan: accel_planner.MovePlan, stepper_cfgs: ty.List[StepperConfig]
) -> ty.List[steppers.Step]:
    all_steps = []
    try:
        for splan, scfg in zip(plan.plan, stepper_cfgs):
            all_steps.extend(plan_to_blocks(splan, scfg))
    except (NoNextStepException, NoStepSolnException):
        now = datetime.datetime.now(dateutil.tz.UTC)

        logging.error(
            "Failure in calculating plan, saving to file [file={}, time={}, plan={} cfg={}]".format(
                cfg.ERRORS_FILE, now, plan, stepper_cfgs
            )
        )
        if cfg.ERRORS_FILE is not None:
            errf = pathlib.Path(cfg.ERRORS_FILE)
            file_size = errf.stat().st_size
            if file_size > 1e7:
                # truncate file if too big
                logging.error("Deleting error file -- too large [file={}]".format(errf))
                open(cfg.ERRORS_FILE, "wb").close()
            with open(cfg.ERRORS_FILE, "ab") as f:
                pickle.dump([now, plan, stepper_cfgs], f)
            return []
    ord_steps = sorted(all_steps, key=lambda s: s.time)
    res = [ord_steps[0]]
    for step in ord_steps[1:]:
        last_step = res[-1]
        next_step_ = attrs.evolve(step, dtime=step.time - last_step.time)
        res.append(next_step_)
    return res
