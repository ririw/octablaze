from octablaze import cfg

if not cfg.DUMMY_GPIO:
    import gpiozero


class Light:
    def __init__(self, light_hot, light_neutral):
        self.light_h = None
        self.light_n = None
        if not cfg.DUMMY_GPIO:
            self.light_h = gpiozero.DigitalOutputDevice(light_hot)
            self.light_n = gpiozero.DigitalOutputDevice(light_neutral)

    def on(self):
        if self.light_h is not None:
            assert self.light_n is not None
            self.light_n.on()
            self.light_h.on()

    def off(self):
        if self.light_h is not None:
            assert self.light_n is not None
            self.light_n.off()
            self.light_h.off()
