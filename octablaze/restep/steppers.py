import logging
import time
import typing as ty

import attr

from octablaze import cfg


class IOInterface:
    def set_direction(self, is_clockwise):
        raise NotImplementedError()

    def energize(self):
        raise NotImplementedError()

    def deenergize(self):
        raise NotImplementedError()

    def step(self):
        raise NotImplementedError()


class DummyDevice:
    def __init__(self, pin):
        self.pin = "DUMMY: " + str(pin)

    def on(self):
        pass

    def off(self):
        pass


class PiInterface(IOInterface):
    def __init__(
        self,
        step_ctrl_pin,
        enable_ctrl_pin,
        clockwise_ctrl_pin,
        clockwise_high=True,
        max_step=80,
    ):
        self.max_step = max_step
        if not cfg.DUMMY_GPIO:
            import gpiozero

            self.step_ctrl = gpiozero.DigitalOutputDevice(step_ctrl_pin)
            self.enable_ctrl = gpiozero.DigitalOutputDevice(
                enable_ctrl_pin, active_high=False
            )
            self.clockwise_ctrl = gpiozero.DigitalOutputDevice(
                clockwise_ctrl_pin, active_high=clockwise_high
            )
            # Sync clockwise ctrl state
            self.clockwise_ctrl.off()
        else:
            self.step_ctrl = DummyDevice(step_ctrl_pin)
            self.enable_ctrl = DummyDevice(enable_ctrl_pin)
            self.clockwise_ctrl = DummyDevice(clockwise_ctrl_pin)
        self._is_clockwise = False
        self.posn = 0

    def set_direction(self, is_clockwise):
        if is_clockwise == self._is_clockwise:
            return
        if is_clockwise:
            self.clockwise_ctrl.on()
            self._is_clockwise = True
        else:
            self.clockwise_ctrl.off()
            self._is_clockwise = False

    def energize(self):
        self.enable_ctrl.on()

    def deenergize(self):
        self.posn = 0
        self.enable_ctrl.off()

    def step(self):
        # convert true / false to 1 / -1
        self.posn += 2 * int(self._is_clockwise) - 1
        if abs(self.posn) >= self.max_step:
            logging.error(
                "Step blocked/unblocked: %s, %s", self.step_ctrl.pin, self.posn
            )
            return
        self.step_ctrl.on()
        self.step_ctrl.off()


class DummyInterface(IOInterface):
    def __init__(self, *args, **kwargs):
        self._is_clockwise = 1
        self._posn = 0

    def set_direction(self, is_clockwise):
        if is_clockwise == self._is_clockwise:
            return
        if is_clockwise:
            self._is_clockwise = 1
        else:
            self._is_clockwise = -1

    def energize(self):
        pass

    def deenergize(self):
        pass

    def step(self):
        self._posn += self._is_clockwise


@attr.s
class StepBlock:
    steps: ty.List["Step"] = attr.ib()
    io_interfaces: ty.Mapping[str, IOInterface] = attr.ib()

    def run_block(self):
        start_time: float = time.perf_counter()
        target_time = 0
        nsteps = 0
        for step in self.steps:
            nsteps += 1
            interface = self.io_interfaces[step.stepper]
            interface.set_direction(step.is_clockwise)
            interface.step()
            target_time += step.dtime
            actual_time = time.perf_counter() - start_time
            if actual_time <= target_time:
                time.sleep(target_time - actual_time)
        actual_time = time.perf_counter() - start_time
        logging.info(
            f"Ran plan [plan length={nsteps} total_time={actual_time} target_tiome={target_time}]"
        )


@attr.s
class Step:
    stepper: str = attr.ib()
    dtime: float = attr.ib()
    time: float = attr.ib()
    is_clockwise: bool = attr.ib()
