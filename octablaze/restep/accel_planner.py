import typing as ty

import attr
import numpy as np
from scipy import interpolate


@attr.s
class PlannerPoint:
    position_mm: np.array = attr.ib()

    def __attrs_post_init__(self):
        self.position_mm = np.array(self.position_mm)


@attr.s
class MovePlan:
    from_point: PlannerPoint = attr.ib()
    to_point: PlannerPoint = attr.ib()
    plan: ty.List["RunPlan"] = attr.ib()


class RunPlan:
    @property
    def a_poly(self) -> interpolate.PPoly:
        raise NotImplementedError()

    @property
    def v_poly(self) -> interpolate.PPoly:
        raise NotImplementedError()

    @property
    def r_poly(self) -> interpolate.PPoly:
        raise NotImplementedError()

    @property
    def t(self) -> float:
        raise NotImplementedError()

    def scale_position(self, factor):
        # type: (float) -> RunPlan
        raise NotImplementedError()


@attr.s
class ShiftedPlan(RunPlan):
    _underlying_plan: RunPlan = attr.ib()
    shift: float = attr.ib()

    @property
    def a_poly(self) -> interpolate.PPoly:
        return self._underlying_plan.a_poly

    @property
    def v_poly(self) -> interpolate.PPoly:
        return self._underlying_plan.v_poly

    @property
    def r_poly(self) -> interpolate.PPoly:
        underpoly = self._underlying_plan.r_poly
        shifted = interpolate.PPoly(
            underpoly.c.copy(), underpoly.x.copy(), extrapolate=False
        )
        shifted.c[-1, :] += self.shift
        return shifted

    @property
    def t(self) -> float:
        return self._underlying_plan.t

    def scale_position(self, factor):
        return ShiftedPlan(
            self._underlying_plan.scale_position(factor), self.shift * factor
        )


@attr.s
class StagedRunPlan(RunPlan):
    initial_accel = attr.ib()
    initial_time = attr.ib()
    coast_time = attr.ib()
    final_accel = attr.ib()
    final_time = attr.ib()

    initial_v = attr.ib()

    def scale_position(self, factor):
        # type: (float) -> StagedRunPlan
        return StagedRunPlan(
            initial_accel=self.initial_accel * factor,
            initial_time=self.initial_time,
            coast_time=self.coast_time,
            final_accel=self.final_accel * factor,
            final_time=self.final_time,
            initial_v=self.initial_v,
        )

    @property
    def a_poly(self):
        c, x = [], [0]
        c.append(self.initial_accel)
        x.append(self.initial_time)
        c.append(0)
        x.append(self.initial_time + self.coast_time)
        c.append(self.final_accel)
        # A tiny bit of wiggle room here ensures we never get np.nan at
        # the value of self.t
        x.append(self.final_time + self.initial_time + self.coast_time + 1e-4)

        c = np.array(c)[None, :]
        x = np.array(x)
        return interpolate.PPoly(c, x, extrapolate=False)

    @property
    def v_poly(self):
        v = self.a_poly.antiderivative()
        v.c[-1, :] += self.initial_v
        return v

    @property
    def r_poly(self):
        return self.v_poly.antiderivative()

    @property
    def t(self):
        return self.initial_time + self.coast_time + self.final_time


@attr.s
class PolyRunPlan(RunPlan):
    _r_poly = attr.ib()
    total_time = attr.ib()

    def scale_position(self, factor):
        return PolyRunPlan(self._r_poly * factor, self.total_time)

    @property
    def a_poly(self):
        return self.r_poly.derivative(2)

    @property
    def r_poly(self):
        lim_poly = interpolate.PPoly(
            self._r_poly.coef[::-1][:, None].copy(), [0, self.t], extrapolate=False
        )
        return lim_poly

    @property
    def v_poly(self):
        return self.r_poly.derivative()

    @property
    def t(self):
        return self.total_time


def accel_to_v(v0, v, max_accel):
    # v = a * t
    # r = a ** 2 / 2 * t
    delta_v = v - v0
    t = delta_v / max_accel
    r = max_accel / 2 * t**2 + t * v0
    if t < 0:
        return -max_accel, -t, -r
    else:
        return max_accel, t, r


def find_t_poly(t, rt, v0, vt):
    vm = np.vstack(
        [
            [1, 1 * t, 1 * t**2, 1 * t**3],  # final pos
            [0, 1, 2 * t, 3 * t**2],  # final vel
            [0, 1, 0, 0],  # Start vel
            [1, 0, 0, 0],  # Start pos (0)
        ]
    )
    vec = np.linalg.solve(vm, [rt, vt, v0, 0])
    r_poly = np.polynomial.Polynomial(vec)
    v_poly = r_poly.deriv()
    a_poly = v_poly.deriv()
    a_max = max(abs(a_poly(np.linspace(0, t, 1000))))
    return r_poly, v_poly, a_max


def goto_r_max(rt, v0, vt, a_max):
    t_max = rt / a_max * 100
    t1, t2 = 1e-10, t_max
    tm = t1
    for step in range(50):
        tm = np.mean([t1, t2])
        a = find_t_poly(tm, rt, v0, vt)[-1]
        if a < a_max:
            t2 = tm
        else:
            t1 = tm
        if t2 - t1 < 1e-6:
            break
    r, v, a = find_t_poly(tm, rt, v0, vt)
    return r, tm


def at_max_accel(
    delta_r_mm: float,
    initial_v_mm_s: float,
    exit_v_mm_s: float,
    max_accel_mm3_s: float,
    feed_rate_mms: float,
):
    ji, ti, ri = accel_to_v(initial_v_mm_s, feed_rate_mms, max_accel_mm3_s)
    jf, tf, rf = accel_to_v(feed_rate_mms, exit_v_mm_s, max_accel_mm3_s)
    if ri + rf < delta_r_mm:
        extra_space = delta_r_mm - (ri + rf)
        return StagedRunPlan(
            ji, ti, extra_space / feed_rate_mms, jf, tf, initial_v_mm_s
        )
    else:
        poly, t = goto_r_max(delta_r_mm, initial_v_mm_s, exit_v_mm_s, max_accel_mm3_s)
        plan = PolyRunPlan(poly, t)
        return plan


def infer_max_rate(
    from_point: PlannerPoint, to_point: PlannerPoint, axis_feed_max_mm_s: float
) -> float:
    # Calc. change in position and find axis that moves fastest
    delta_r = from_point.position_mm - to_point.position_mm
    if np.linalg.norm(delta_r, 2) == 0:
        return axis_feed_max_mm_s
    max_axis = np.max(abs(delta_r))
    max_axis_ix = np.argmax(abs(delta_r))
    # Then, scale feed rate to that max axis, such that we
    # can then scale delta R by this scaling, and we will
    # end up w/ a vector where that max axis takes value of
    # feed rate
    feed_scaling = axis_feed_max_mm_s / max_axis
    delta_v = delta_r * feed_scaling
    assert np.isclose(abs(delta_v[max_axis_ix]), axis_feed_max_mm_s, 10)
    # Finally, calc. the feed rate as the euclidan norm
    return np.linalg.norm(delta_v, 2)


def plan_from_to(
    from_point: PlannerPoint,
    to_point: PlannerPoint,
    feed_rate_mms: float,
    max_accel_mm2_s: float,
) -> MovePlan:
    if feed_rate_mms <= 0:
        raise ValueError(f"Feed rate must be greater than 0 [rate={feed_rate_mms}]")
    if max_accel_mm2_s <= 0:
        raise ValueError(f"Accel must be greater than 0 [rate={max_accel_mm2_s}]")

    distance_mm = np.linalg.norm(from_point.position_mm - to_point.position_mm, 2)
    max_speed = infer_max_rate(from_point, to_point, feed_rate_mms)
    max_accel = infer_max_rate(from_point, to_point, max_accel_mm2_s)
    assert max_accel >= 0
    assert max_speed >= 0
    plan = at_max_accel(distance_mm, 0, 0, max_accel_mm2_s, max_speed)

    dr = to_point.position_mm - from_point.position_mm
    dist = np.linalg.norm(dr)
    if dist == 0:
        dist = 1  # For cases where from == to, we just ignore scaling.
    axis_scalings = dr / dist
    axis_plans = []
    for axis_scale, start, endp in zip(
        axis_scalings, from_point.position_mm, to_point.position_mm
    ):
        axis_plan = ShiftedPlan(plan.scale_position(axis_scale), start)
        axis_plans.append(axis_plan)

    final_t = axis_plans[0].t
    final_point = [p.r_poly(final_t) for p in axis_plans]
    np.testing.assert_almost_equal(final_point, to_point.position_mm, 4)

    return MovePlan(from_point, to_point, axis_plans)
