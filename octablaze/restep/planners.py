"""
Some code taken from robotics-toolbox-python, licenced under MIT licence.

MIT License

Copyright (c) 2020 jhavl

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""
import numpy as np


class Trajectory:
    """
    A container class for trajectory data.
    """

    def __init__(self, name, t, s, sd=None, sdd=None, istime=False):
        """
        Construct a new trajectory instance

        :param name: name of the function that created the trajectory
        :type name: str
        :param t: independent variable, eg. time or step
        :type t: ndarray(m)
        :param s: position
        :type s: ndarray(m) or ndarray(m,n)
        :param sd: velocity
        :type sd: ndarray(m) or ndarray(m,n)
        :param sdd: acceleration
        :type sdd: ndarray(m) or ndarray(m,n)
        :param istime: ``t`` is time, otherwise step number
        :type istime: bool

        The object has attributes:

        - ``t``  the independent variable
        - ``s``  the position
        - ``sd``  the velocity
        - ``sdd``  the acceleration

        If ``t`` is time, ie. ``istime`` is True, then the units of ``sd`` and
        ``sdd`` are :math:`s^{-1}` and :math:`s^{-2}` respectively, otherwise
        with respect to ``t``.

        .. note:: Data is stored with timesteps as rows and axes as columns.
        """
        self.name = name
        self.t = t
        self.s = s
        self.sd = sd
        self.sdd = sdd
        self.istime = istime

    def __str__(self):
        s = f"Trajectory created by {self.name}: {len(self)} time steps x {self.naxes} axes"
        return s

    def __repr__(self):
        return str(self)

    def __len__(self):
        """
        Length of trajectory

        :return: number of steps in the trajectory
        :rtype: int
        """
        return self.s.shape[0]

    def q(self):
        """
        Position trajectory

        :return: trajectory with one row per timestep, one column per axis
        :rtype: ndarray(n,m)

        .. note:: This is a synonym for ``.s``, for compatibility with other
            applications.
        """
        return self.s

    def qd(self):
        """
        Velocity trajectory

        :return: trajectory velocity with one row per timestep, one column per axis
        :rtype: ndarray(n,m)

        .. note:: This is a synonym for ``.sd``, for compatibility with other
            applications.
        """
        return self.sd

    def qdd(self):
        """
        Acceleration trajectory

        :return: trajectory acceleration with one row per timestep, one column per axis
        :rtype: ndarray(n,m)

        .. note:: This is a synonym for ``.sdd``, for compatibility with other
            applications.
        """
        return self.sdd

    @property
    def naxes(self):
        """
        Number of axes in the trajectory

        :return: number of axes or dimensions
        :rtype: int
        """
        if self.s.ndim == 1:
            return 1
        else:
            return self.s.shape[1]


def lspb_func(q0, qf, tf, v=None):
    if v is None:
        # if velocity not specified, compute it
        v = (qf - q0) / tf * 1.5
    else:
        v = abs(v) * np.sign(qf - q0)
        if abs(v) < (abs(qf - q0) / tf):
            raise ValueError("V too small")
        elif abs(v) > (2 * abs(qf - q0) / tf):
            raise ValueError("V too big")

    tb = (q0 - qf + v * tf) / v
    a = v / tb
    if np.isnan(tb):
        tb = 0
        a = 0

    def lspbfunc(t):
        p = []
        pd = []
        pdd = []

        if np.isscalar(t):
            t = [t]

        for tk in t:
            if tk <= tb:
                # initial blend
                pk = q0 + a / 2 * tk**2
                pdk = a * tk
                pddk = a
            elif tk <= (tf - tb):
                # linear motion
                pk = (qf + q0 - v * tf) / 2 + v * tk
                pdk = v
                pddk = 0
            else:
                # final blend
                pk = qf - a / 2 * tf**2 + a * tf * tk - a / 2 * tk**2
                pdk = a * tf - a * tk
                pddk = -a
            p.append(pk)
            pd.append(pdk)
            pdd.append(pddk)
        return np.array(p), np.array(pd), np.array(pdd)

    # return the function, but add some computed parameters as attributes
    # as a way of returning extra values without a tuple return
    func = lspbfunc
    func.tb = tb
    func.V = v

    return func


def lspb(q0, qf, t, v=None):
    tf = max(t)
    lspbfunc = lspb_func(q0, qf, tf, v)
    istime = True

    traj = lspbfunc(t)
    p = traj[0]
    pd = traj[1]
    pdd = traj[2]

    traj = Trajectory("lspb", t, p, pd, pdd, istime)
    # noinspection PyUnresolvedReferences
    traj.tblend = lspbfunc.tb
    return traj


def mtraj(tfunc, q0, qf, t):
    if not callable(tfunc):
        raise TypeError("first argument must be a function reference")

    q0 = np.array(q0)
    qf = np.array(qf)
    if q0.shape != qf.shape:
        raise ValueError("must be same number of elements in q0 and qf")

    traj = []
    for i in range(q0.shape[0]):
        # for each axis
        traj.append(tfunc(q0[i], qf[i], t))

    x = traj[0].t
    y = np.array([tg.s for tg in traj]).T
    yd = np.array([tg.sd for tg in traj]).T
    ydd = np.array([tg.sdd for tg in traj]).T

    istime = traj[0].istime

    return Trajectory("mtraj", x, y, yd, ydd, istime)
