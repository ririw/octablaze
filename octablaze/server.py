import asyncio
import logging
import threading
import time
import typing as ty
from concurrent.futures import ThreadPoolExecutor
from enum import Enum

import numpy as np
from fastapi import FastAPI
from pydantic import BaseModel
from starlette import status
from starlette.responses import Response

from octablaze.restep import (
    accel_planner,
    blocker,
    light_ctrl,
    radial_planner,
    steppers,
)

app = FastAPI()

nav_logger = logging.getLogger("nav_logger")


class Activity(str, Enum):
    static = "static"
    breathing = "breathing"
    off = "off"
    on = "on"
    random = "random"


class State(BaseModel):
    activity: Activity
    is_on: bool


class StateUpdate(BaseModel):
    activity: ty.Optional[Activity]
    is_on: ty.Optional[bool]


octastate = State(activity=Activity.static, is_on=False)
next_state: asyncio.Queue[State] = asyncio.Queue(maxsize=1)
state_lock = asyncio.Lock()
stepper_interfaces = {
    "ns": steppers.PiInterface(
        step_ctrl_pin=4,  # Yellow
        enable_ctrl_pin=3,  # Green
        clockwise_ctrl_pin=2,  # Red
        clockwise_high=False,
        max_step=200,
    ),
    "ew": steppers.PiInterface(
        step_ctrl_pin=15,  # white
        enable_ctrl_pin=14,  # grey
        clockwise_ctrl_pin=18,  # black
        clockwise_high=False,
        max_step=200,
    ),
}
light = light_ctrl.Light(20, 21)
light.off()


@app.get("/", response_model=State)
async def root() -> State:
    async with state_lock:
        return octastate.copy()


@app.post("/", response_model=State)
@app.post("/activity", response_model=State)
async def set_state(state_update: StateUpdate, response: Response) -> State:
    new_state = await update_state(state_update)
    if new_state is None:
        response.status_code = status.HTTP_429_TOO_MANY_REQUESTS
        async with state_lock:
            return octastate.copy()
    else:
        # noinspection PyTypeChecker
        return new_state


async def update_state(state_update: StateUpdate) -> ty.Optional[State]:
    global octastate
    async with state_lock:
        new_state = octastate.copy()
        if state_update.activity is not None:
            new_state.activity = state_update.activity
        if state_update.is_on is not None:
            new_state.is_on = state_update.is_on

        try:
            next_state.put_nowait(new_state)
            return new_state
        except asyncio.QueueFull:
            return None


@app.post("/activity", response_model=State)
async def set_activity(activity: Activity, response: Response) -> State:
    new_state = update_state(StateUpdate(activity=activity))
    if new_state is not None:
        response.status_code = status.HTTP_429_TOO_MANY_REQUESTS
        async with state_lock:
            return octastate.copy()
    else:
        # noinspection PyTypeChecker
        return new_state


@app.on_event("startup")
async def start_octablaze():
    asyncio.get_event_loop().create_task(run_blaze())


_exit = False


def off():
    logging.info("Running off activity")
    for val in stepper_interfaces.values():
        val.deenergize()
    light.off()
    static()


def on():
    logging.info("Running off activity")
    light.on()
    static()


def static():
    logging.info("Running static activity")
    while True:
        time.sleep(1)
        if _exit:
            logging.info("Finished static activity")
            return


# noinspection DuplicatedCode
def breathing():
    light.on()
    for val in stepper_interfaces.values():
        val.energize()
    while True:
        wire = radial_planner.WireConfig(10, 25, 20, 0.5, 2)
        stepper_cfg = [
            blocker.StepperConfig(0, "ns", True, 200),
            blocker.StepperConfig(0, "ew", True, 200),
        ]
        steps = radial_planner.steps_to_posn(
            radial_planner.AnglePosition(0, 0),
            radial_planner.AnglePosition(3, 3),
            wire,
            stepper_cfg,
        )
        steppers.StepBlock(steps, stepper_interfaces).run_block()
        if _exit:
            break
        steps = radial_planner.steps_to_posn(
            radial_planner.AnglePosition(3, 3),
            radial_planner.AnglePosition(-3, -3),
            wire,
            stepper_cfg,
        )
        steppers.StepBlock(steps, stepper_interfaces).run_block()
        steps = radial_planner.steps_to_posn(
            radial_planner.AnglePosition(-3, -3),
            radial_planner.AnglePosition(0, 0),
            wire,
            stepper_cfg,
        )
        steppers.StepBlock(steps, stepper_interfaces).run_block()

        if _exit:
            break
    for val in stepper_interfaces.values():
        val.deenergize()


def random():
    light.on()
    for val in stepper_interfaces.values():
        val.energize()
    while True:
        stepper_cfg = [
            blocker.StepperConfig(0, "ns", True, 200),
            blocker.StepperConfig(0, "ew", True, 200),
        ]

        positions = np.random.uniform(-1, 1, size=(5, 2))
        positions[-1, :] = 0
        last_posn = 0, 0
        for posn in positions:
            lx, ly = last_posn
            px, py = posn
            _navto([lx, ly], [px, ly], stepper_cfg, 0.5, 2)
            _navto([px, ly], [px, py], stepper_cfg, 0.5, 2)
            last_posn = posn

            if _exit:
                break

        if _exit:
            break

    # Return us to zero
    for val in stepper_interfaces.values():
        val.deenergize()
    time.sleep(2)


def _navto(last_posn, posn, stepper_cfg, max_speed_mm_s, max_accel_mm_ss):
    nav_logger.error("Nav from {} to {}".format(last_posn, posn))
    plan = accel_planner.plan_from_to(
        accel_planner.PlannerPoint(last_posn),
        accel_planner.PlannerPoint(posn),
        max_speed_mm_s,
        max_accel_mm_ss,
    )
    steps = blocker.fully_plan(plan, stepper_cfg)
    steppers.StepBlock(steps, stepper_interfaces).run_block()


async def run_blaze():
    global octastate, _exit
    loop = asyncio.get_event_loop()
    # put our main task in the background as a daemon thread.
    # It would be nice to put it into the event loop, but we
    # really need this to be in daemon mode, and I can't work
    # out how to do that with the executor loops.
    current_thread = threading.Thread(target=static, daemon=True)
    current_thread.start()
    with ThreadPoolExecutor(1) as pool:
        while True:
            # Wait for a new state and pick new task based on it
            new_state = await next_state.get()

            if new_state.activity == Activity.static:
                next_task = static
            elif new_state.activity == Activity.breathing:
                next_task = breathing
            elif new_state.activity == Activity.random:
                next_task = random
            elif new_state.activity == Activity.off:
                next_task = off
            elif new_state.activity == Activity.on:
                next_task = on
            else:
                raise NotImplementedError(
                    f"Unknown activity [activity={new_state.activity}]"
                )

            # Cause task to exit. We do the thread join in
            # an executor so that the join call won't block
            # our main asyncIO thread.
            _exit = True
            await loop.run_in_executor(pool, current_thread.join)
            _exit = False
            # Start the next thread.
            current_thread = threading.Thread(target=next_task, daemon=True)
            current_thread.start()
            async with state_lock:
                octastate = new_state
