import os

from decouple import config

ERRORS_FILE = config("ERRORS_FILE", default=None)
DUMMY_GPIO = (
    config("DUMMY_GPIO", default=True, cast=bool) or "riri" in os.environ["USER"]
)
