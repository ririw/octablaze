#include <ArduinoJson.h>
#include "TeensyTimerTool.h"
using namespace TeensyTimerTool;

typedef struct PinState
{
  uint8_t pin;
  bool is_output;
  bool in_use;
} PinState;

typedef struct Action
{
  uint_fast32_t microdelta;
  uint8_t pin;
  bool is_high;
} Action;

const int NUM_CELLS = 1024;

typedef struct Actions
{
  size_t cells_used;
  size_t cells_run;
  bool force;
  Action cells[NUM_CELLS];
} Actions;

bool read_pin_states(PinState available_pins[], unsigned int num_pins)
{
  StaticJsonDocument<500> doc;
  String content;

  // Blank all pins
  for (uint8_t pin_ix = 0; pin_ix < num_pins; pin_ix++)
  {
    available_pins[pin_ix].in_use = false;
    available_pins[pin_ix].pin = pin_ix;
  }

  // Look for some non-empty content starting with
  // CONFIGURE or RESET
  content = Serial.readStringUntil('\n');
  Serial.println("AWAITING CONFIG");
  if (content == "RESET")
  {
    _reboot_Teensyduino_();
  }
  if (content != "CONFIGURE")
  {
    Serial.print("Expected CONFIGURE got ");
    Serial.println(content);
    return false;
  }

  // Read in more content in a loop, waiting for "END"
  content = Serial.readStringUntil('\n');
  while (content != "END")
  {
    // Pull the json, exit if there's a bad value.
    auto err = deserializeJson(doc, content);
    if (err != DeserializationError::Ok)
    {
      Serial.printf("ERROR: in deserialisation of config: %s\n", content.c_str());
      return false;
    }
    unsigned int pin_ix = doc["pin_ix"].as<int>();
    if (pin_ix < 0 || pin_ix >= num_pins)
    {
      Serial.print("ERROR: Pin IX exceeds safe limits in ");
      Serial.println(content);
      return false;
    }
    available_pins[pin_ix].is_output = doc["is_output"].as<bool>();
    available_pins[pin_ix].in_use = true;
    // Read more content and loop again
    content = Serial.readStringUntil('\n');
  }
  // If we got this far, we know we succeeded.
  return true;
}

void apply_pin_states(PinState available_pins[], int num_pins)
{
  for (int pin_ix = 0; pin_ix < num_pins; pin_ix++)
  {
    if (available_pins[pin_ix].in_use)
    {
      if (available_pins[pin_ix].is_output)
      {
        Serial.print("INFO: Setting pin to output: ");
        Serial.println(pin_ix);
        pinMode(pin_ix, OUTPUT);
      }
      else
      {
        Serial.println("ERROR: Input not yet supported");
      }
    }
  }
}

bool read_activity_message(Actions &actions)
{
  Serial.println("READING>");
  Serial.flush();
  StaticJsonDocument<500> doc;
  Serial.setTimeout(500);
  String content = Serial.readStringUntil('\n');
  actions.cells_used = 0;
  actions.cells_run = 0;
  actions.force = 0;

  if (content == "RESET")
  {
    _reboot_Teensyduino_();
  }
  if (content == "SETUP")
  {
    setup_pins();
    return false;
  }
  if (content != "START" && content != "START FORCED")
  {
    // Serial.printf("Expected block start, got %s\n", content.c_str());
    return false;
  }
  if (content == "START FORCED")
  {
    actions.force = true;
  }

  for (unsigned int ptr = 0; ptr < NUM_CELLS; ptr++)
  {
    String content = Serial.readStringUntil('\n');
    if (content == "END")
    {
      return true;
    }
    DeserializationError err = deserializeJson(doc, content);
    if (err != DeserializationError::Ok)
    {
      Serial.printf("ERROR: in deserialisation of %s\n", content.c_str());
      actions.cells_used = 0;
      actions.cells_run = 0;
      actions.force = 0;
      return false;
    }

    auto as_arr =doc.as<JsonArray>();

    int delta = as_arr[2].as<int>();
    int is_high = doc[1].as<bool>();
    int pin = doc[0].as<int>();
    actions.cells[actions.cells_used].microdelta = delta;
    actions.cells[actions.cells_used].pin = pin;
    actions.cells[actions.cells_used].is_high = is_high;
    actions.cells_used++;
  }

  actions.force = 0;
  actions.cells_used = 0;
  actions.cells_run = 0;
  Serial.println("ERROR: Action size overflow; resetting");
  return false;
}

Actions actions_block = {0};
Actions next_block = {0};
Actions blocks[2] = {0};
bool timer_running = false;
OneShotTimer action_timer;

bool attempt_move_next_block(Actions &actions_block, Actions &next_block)
{
  // No next block, so cancel the timer.
  if (next_block.cells_run >= next_block.cells_used)
  {
    Serial.println("INFO: No block");
    timer_running = false;
    action_timer.stop();
    return false;
  }
  // Next block exists, so we set it up and start running.
  else
  {
    Serial.println("INFO: New block");
    memcpy(&actions_block, &next_block, sizeof(Actions));
    next_block.cells_run = 0;
    next_block.cells_used = 0;
    next_block.force = 0;
    return true;
  }
}

void apply_next_action()
{
  noInterrupts();

  if (actions_block.cells_run >= actions_block.cells_used)
  {
    attempt_move_next_block(actions_block, next_block);
  }

  while (actions_block.cells_run < actions_block.cells_used)
  {
    Action action = actions_block.cells[actions_block.cells_run];
    digitalWrite(action.pin, action.is_high ? HIGH : LOW);
    // Serial.println(action.is_high);
    actions_block.cells_run++;

    if (action.microdelta > 0)
    {
      timer_running = true;
      errorCode err = action_timer.trigger(action.microdelta);
      if (errorCode::OK != err)
      {
        Serial.print("ERROR: Unable to schedule action: ");
        Serial.println((int)err);
      }
      // Serial.printf("ACTION - %d / %d / %d / %d\n", actions_block.cells_used, actions_block.cells_run, action.microdelta, success);
      break;
    }
  }

  interrupts();
}

bool queue_actions(Actions &actions)
{
  noInterrupts();
  if (actions.force)
  {
    timer_running = false;
    action_timer.stop();
    actions_block.cells_run = actions_block.cells_used;
    actions_block.force = false;
    memcpy(&next_block, &actions, sizeof(Actions));
    // Once we apply the action, the next block will be slotted
    // into the actions block as desired.
    timer_running = true;
    apply_next_action();
    interrupts();
    return true;
  }
  else
  {
    if (next_block.cells_run < next_block.cells_used)
    {
      interrupts();
      Serial.printf("ERROR: block queued already %d %d\n", next_block.cells_used, next_block.cells_run);
      return false;
    }
    memcpy(&next_block, &actions, sizeof(Actions));

    if (!timer_running)
    {
      timer_running = true;
      apply_next_action();
    }
    interrupts();
  }
  return true;
}

void setup()
{
  Serial.begin(115200);
  while (!Serial)
  {
    delay(100);
  }
  Serial.println("READY");
  Serial.flush();

  setup_pins();
  action_timer.begin(apply_next_action);
}

void setup_pins() {
  const int num_pin_states = 32;
  PinState pin_states[num_pin_states];
  do
  {
  } while (!read_pin_states(pin_states, num_pin_states));

  apply_pin_states(pin_states, num_pin_states);
}

void print_status(Actions &running_buffer, Actions &next_buffer)
{
  int running_block_steps, running_block_run, next_block_steps, next_block_run;
  running_block_steps = running_buffer.cells_used;
  running_block_run = running_buffer.cells_run;
  next_block_steps = next_buffer.cells_used;
  next_block_run = next_buffer.cells_run;
  Serial.printf("STATUS: %d %d %d %d\n", running_block_steps, running_block_run, next_block_steps, next_block_run);
}

void loop()
{
  static Actions actions_buffer = {0};
  bool res = read_activity_message(actions_buffer);

  if (res)
  {
    bool qres = queue_actions(actions_buffer);
    if (qres == false)
    {
      Serial.println("ERROR: failed to queue block");
    }
    else
    {
      Serial.println("INFO: block queued");
    }
  }
  print_status(actions_block, next_block);
}