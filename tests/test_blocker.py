from octablaze.restep import accel_planner, blocker


def test_single_blocking():
    p1 = accel_planner.PlannerPoint([0])
    p2 = accel_planner.PlannerPoint([1])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = blocker.StepperConfig(0, "x", False, 100)
    blocks = blocker.plan_to_blocks(plan.plan[0], config)
    assert len(blocks) == 100


def test_long_blocking():
    p1 = accel_planner.PlannerPoint([0])
    p2 = accel_planner.PlannerPoint([1])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = blocker.StepperConfig(0, "x", False, 500)
    blocks = blocker.plan_to_blocks(plan.plan[0], config)
    assert len(blocks) == 500


def test_slow_long_blocking():
    p1 = accel_planner.PlannerPoint([0])
    p2 = accel_planner.PlannerPoint([90])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = blocker.StepperConfig(0, "x", False, 100)
    blocks = blocker.plan_to_blocks(plan.plan[0], config)
    assert len(blocks) == 100 * 90


# noinspection DuplicatedCode
def test_full_plan_simple():
    p1 = accel_planner.PlannerPoint([0, 0])
    p2 = accel_planner.PlannerPoint([0, 90])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = [
        blocker.StepperConfig(0, "x", False, 100),
        blocker.StepperConfig(0, "y", False, 100),
    ]
    blocks = blocker.fully_plan(plan, config)
    assert len(blocks) == 100 * 90


# noinspection DuplicatedCode
def test_full_plan_simple_2():
    p1 = accel_planner.PlannerPoint([0, 30])
    p2 = accel_planner.PlannerPoint([0, 90])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = [
        blocker.StepperConfig(0, "x", False, 100),
        blocker.StepperConfig(0, "y", False, 100),
    ]
    blocks = blocker.fully_plan(plan, config)
    assert len(blocks) == 100 * 60


# noinspection DuplicatedCode
def test_full_plan_diag():
    p1 = accel_planner.PlannerPoint([0, 0])
    p2 = accel_planner.PlannerPoint([40, 20])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = [
        blocker.StepperConfig(0, "x", False, 100),
        blocker.StepperConfig(0, "y", False, 100),
    ]
    blocks = blocker.fully_plan(plan, config)
    assert len(blocks) == 100 * 60


# noinspection DuplicatedCode
def test_full_plan_diag_bad():
    p1 = accel_planner.PlannerPoint([0, 0])
    p2 = accel_planner.PlannerPoint([40, 30])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2

    config = [
        blocker.StepperConfig(0, "x", False, 100),
        blocker.StepperConfig(0, "y", False, 100),
    ]
    blocks = blocker.fully_plan(plan, config)
    assert len(blocks) == 100 * 70
