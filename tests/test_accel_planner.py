import numpy as np

from octablaze.restep import accel_planner


def _check_accel_plan(p1, p2):
    accel1, dt1, dr1 = p1
    accel2, dt2, dr2 = p2
    assert accel1 == accel2
    np.testing.assert_almost_equal(dt1, dt2, 10)
    np.testing.assert_almost_equal(dr1, dr2, 10)


def test_simple_accel_to_v():
    plan = accel_planner.accel_to_v(0, 10, 100)
    expected = 100, 10 / 100, (10 / 100) ** 2 * 100 / 2
    _check_accel_plan(plan, expected)

    plan = accel_planner.accel_to_v(-10, 0, 100)
    expected = 100, 10 / 100, -((10 / 100) ** 2) * 100 / 2
    _check_accel_plan(plan, expected)


def test_simple_accel_to_vneg():
    plan = accel_planner.accel_to_v(0, -10, 100)
    expected = -100, 10 / 100, -((10 / 100) ** 2) * 100 / 2
    _check_accel_plan(plan, expected)

    plan = accel_planner.accel_to_v(10, 0, 100)
    expected = -100, 10 / 100, (10 / 100) ** 2 * 100 / 2
    _check_accel_plan(plan, expected)


def test_simple_accel_to_zero():
    plan = accel_planner.accel_to_v(0, 0, 100)
    expected = 100, 0, 0
    _check_accel_plan(plan, expected)


def test_simple_accel_to_zero_delta():
    plan = accel_planner.accel_to_v(10, 10, 100)
    expected = 100, 0, 0
    _check_accel_plan(plan, expected)


def test_at_max_accel_simple_short():
    plan = accel_planner.at_max_accel(3, 0, 0, 100, 100)
    assert "PolyRunPlan" in plan.__class__.__name__
    np.testing.assert_almost_equal(plan.r_poly(0), 0, 8)
    np.testing.assert_almost_equal(plan.r_poly(plan.t), 3, 8)
    np.testing.assert_almost_equal(plan.v_poly(0), 0, 8)
    np.testing.assert_almost_equal(plan.v_poly(plan.t), 0, 8)
    max_v = np.max(abs(plan.v_poly(np.linspace(0, plan.t, 1000))))
    max_a = np.max(abs(plan.a_poly(np.linspace(0, plan.t, 1000))))
    assert max_v <= 100 * 2  # add wiggle room, as this approach ignores max speed
    assert max_a <= 100


def test_at_max_accel_simple_long():
    plan = accel_planner.at_max_accel(1000, 0, 0, 100, 5)
    assert "StagedRunPlan" in plan.__class__.__name__
    np.testing.assert_almost_equal(plan.r_poly(0), 0, 10)
    np.testing.assert_almost_equal(plan.r_poly(plan.t), 1000, 5)
    np.testing.assert_almost_equal(plan.v_poly(0), 0, 10)
    np.testing.assert_almost_equal(plan.v_poly(plan.t), 0, 10)
    max_v = np.nanmax(abs(plan.v_poly(np.linspace(0, plan.t, 1000))))
    max_a = np.nanmax(abs(plan.a_poly(np.linspace(0, plan.t, 1000))))
    assert max_v <= 5
    assert max_a <= 100


def test_infer_feed_rate():
    p1 = accel_planner.PlannerPoint([1, 2, 3])
    p2 = accel_planner.PlannerPoint([1, 2, 3])
    assert accel_planner.infer_max_rate(p1, p2, 666) == 666

    p3 = accel_planner.PlannerPoint([1, 2, 10])
    assert accel_planner.infer_max_rate(p1, p3, 666) == 666

    p4 = accel_planner.PlannerPoint([1, 9, 10])
    np.testing.assert_almost_equal(
        accel_planner.infer_max_rate(p1, p4, 666), 666 * np.sqrt(2), 8
    )

    p5 = accel_planner.PlannerPoint([1, 2 - 10, 3 - 10])
    np.testing.assert_almost_equal(
        accel_planner.infer_max_rate(p1, p5, 666), 666 * np.sqrt(2), 8
    )


def test_plan_scaling_small():
    plan = accel_planner.at_max_accel(3, 0, 0, 100, 100)
    assert "PolyRunPlan" in plan.__class__.__name__
    _check_scaled_plan(plan)


def test_plan_scaling_large():
    plan = accel_planner.at_max_accel(1000, 0, 0, 100, 5)
    assert "StagedRunPlan" in plan.__class__.__name__
    _check_scaled_plan(plan)


def _check_scaled_plan(plan):
    scaled_plan = plan.scale_position(0.5)
    assert np.isclose(scaled_plan.t, plan.t)
    np.testing.assert_almost_equal(scaled_plan.r_poly(0), 0)
    t = plan.t
    np.testing.assert_almost_equal(scaled_plan.r_poly(t) * 2, plan.r_poly(t))


def check_plan_constraints(plan: accel_planner.MovePlan, max_v, max_a):
    t = plan.plan[0].t
    ts = np.linspace(0, t, 10000)

    r0 = [p.r_poly(0) for p in plan.plan]
    rt = [p.r_poly(t) for p in plan.plan]
    v_maxes = [np.max(abs(p.v_poly(ts))) for p in plan.plan]
    a_maxes = [np.max(abs(p.a_poly(ts))) for p in plan.plan]
    np.testing.assert_almost_equal(plan.from_point.position_mm, r0)
    np.testing.assert_almost_equal(plan.to_point.position_mm, rt)
    assert np.max(v_maxes) <= max_v + 0.0001
    assert np.max(a_maxes) <= max_a + 0.0001


def test_infer_plan_0():
    p1 = accel_planner.PlannerPoint([0, 0, 0])
    p2 = accel_planner.PlannerPoint([0, 0, 0])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2
    check_plan_constraints(plan, 100, 10)


def test_infer_plan_1():
    p1 = accel_planner.PlannerPoint([0, 0, 0])
    p2 = accel_planner.PlannerPoint([10, 0, 0])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2
    check_plan_constraints(plan, 100, 10)


def test_infer_plan_2():
    p1 = accel_planner.PlannerPoint([5, 5, 0])
    p2 = accel_planner.PlannerPoint([10, 10, 0])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2
    check_plan_constraints(plan, 100, 10)


def test_infer_plan_3():
    p1 = accel_planner.PlannerPoint([5, 5, 0])
    p2 = accel_planner.PlannerPoint([-10, -10, 0])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2
    check_plan_constraints(plan, 100, 10)


def test_infer_plan_4():
    p1 = accel_planner.PlannerPoint([1, 2, 3])
    p2 = accel_planner.PlannerPoint([1, 2 - 10, 3 - 10])

    plan = accel_planner.plan_from_to(p1, p2, 100, 10)
    assert plan.from_point is p1
    assert plan.to_point is p2
    check_plan_constraints(plan, 100, 10)
